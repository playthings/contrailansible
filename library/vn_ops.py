#!/bin/env python

DOCUMENTATION = '''
---
module: vn_ops
short_description: Manage Contrail Virtual Networks (add +/- route-target/network policy/subnet)
'''

EXAMPLES = '''
- name: Create or Update VN
  vn_ops:
   user: "admin"
   pass: "dolce-far-niente"
   contrail_api_url: "10.91.121.62"
   keystone_public: "10.91.121.62"
   keystone_port: "5000"
   tenant: "admin"
   vn_name: "TEST-VN"
   state: present
  register: debugvar 
  no_log: true

- name: Delete a VN
  vn_ops:
   user: "admin"
   pass: "dolce-delete-niente"
   contrail_api_url: "10.91.121.62"
   keystone_public: "10.91.121.62"
   keystone_port: "5000"
   tenant: "admin"
   vn_name: "TEST-VN"
   state: absent
  register: debugvar
  no_log: true

- debug: var=debugvar
'''
# PASS STRUCTURE OF VARIABLES (only variables in reality) as reusable parameters to ansible playbook calls
# - hosts: localhost
#  vars:
#        contrail_login: &contrail_login
#         user: "admin"
#         pass: "fVpuDrAGMpNyb98T8szJxu4B4"
#         contrail_api_url: "10.91.121.62"
#         keystone_public: "10.91.121.62"
#         keystone_port: "5000"
#
#- hosts: localhost
#  tasks:
#    - name: Test that my module works
#      vn_ops:
#       <<: *contrail_login
#       tenant: "admin"
#       vn_name: "TEST-MIHAI"
#       state: present
#      register: result

from ansible.module_utils.basic import *
from cfgm_common.exceptions import *
from vnc_api import vnc_api
import json 

def check_vn(vnc,tenant,vn):
    try:
        vnc.virtual_network_read(fq_name = ['default-domain', tenant, vn])
    except Exception as exp:
        return False
    else:
        return True

def create_vn(vnc,data):
    try:
        tenant_obj = vnc.project_read(fq_name = ['default-domain', data['tenant']])
        vn=vnc_api.VirtualNetwork(name = data['vn_name'], parent_obj = tenant_obj)
        vnc.virtual_network_create(vn)
    except RefsExistError as exp:
        pass
        return 0
    except Exception as exp:
        exit_module(False, False, "This error: " + str(type(exp)) + " has occured " + exp.__str__())
    else:
        return 1

def update_rt(vnc,data):
    if data['rt_import']:
        try:
            route_targets = vnc_api.RouteTargetList(data['rt_import'])
            vn=vnc.virtual_network_read(fq_name = ['default-domain', data['tenant'], data['vn_name']])
            vn.set_import_route_target_list(route_targets)
            vnc.virtual_network_update(vn)
        except Exception as exp:
            exit_module(True, False, "This error: " + str(type(exp)) + " has occured " + exp.__str__())

    if data['rt_export']:
        try:
            route_targets = vnc_api.RouteTargetList(data['rt_export'])
            vn=vnc.virtual_network_read(fq_name = ['default-domain', data['tenant'], data['vn_name']])
            vn.set_export_route_target_list(route_targets)
            vnc.virtual_network_update(vn)
        except Exception as exp:
            exit_module(True, False, "This error: " + str(type(exp)) + " has occured " + exp.__str__())


def update_netpol(vnc,data):
    for netpol in data['network_policy']:
        try:
            vn=vnc.virtual_network_read(fq_name = ['default-domain', data['tenant'], data['vn_name']])
            policy_object = vnc.network_policy_read(fq_name = ['default-domain', data['tenant'], netpol])
            vn.add_network_policy(policy_object,vnc_api.VirtualNetworkPolicyType(sequence=vnc_api.SequenceType(0,0)))
            vnc.virtual_network_update(vn)
        except Exception as exp:
            exit_module(True, False, "This error: " + str(type(exp)) + " has occured " + exp.__str__())

def update_subnet(vnc,data):
        subnets=data['subnet']
        try:
            vn=vnc.virtual_network_read(fq_name = ['default-domain', data['tenant'], data['vn_name']])
            ipam = vnc.network_ipam_read(fq_name = ['default-domain',
                                                    'default-project',
                                                    data['ipam']])
            vn=vnc.virtual_network_read(fq_name = ['default-domain', data['tenant'], data['vn_name']])
            vn_subnets = [vnc_api.IpamSubnetType(
                            subnet = vnc_api.SubnetType(
                                        ip_prefix = subnet.split('/')[0],
                                        ip_prefix_len = subnet.split('/')[1]),
                            ) for subnet in data['subnet']]
            if not vn.get_network_ipam_refs():
                vn.set_network_ipam(ref_obj = ipam,
                        ref_data = vnc_api.VnSubnetsType(vn_subnets))
                vnc.virtual_network_update(vn)
                return " Subnet "
            else:
                list_temp=[]
                for i in range(len(vn.get_network_ipam_refs())):
                    for j in range(len(vn.get_network_ipam_refs()[i]['attr'].get_ipam_subnets())):
                        list_temp.append(vn.get_network_ipam_refs()[i]['attr'].get_ipam_subnets()[j].subnet.ip_prefix + 
                            "/" + vn.get_network_ipam_refs()[i]['attr'].get_ipam_subnets()[i].subnet.ip_prefix_len)
                list_temp.sort()
                subnets.sort()
                subnets_un = [ unicode(item) for item in subnets]
                if not subnets == list_temp:
                    vn.set_network_ipam(ref_obj = ipam, 
                        ref_data = vnc_api.VnSubnetsType(vn_subnets))
                    vnc.virtual_network_update(vn)
                    return " Subnet "
                return ""
        except Exception as exp:
            exit_module(True, False, "This error: " + str(type(exp)) + " has occured " + exp.__str__())

def check(vnc,data,created=0):
    string="VN created successfully" if created==1 else ""
    vn=vnc.virtual_network_read(fq_name = ['default-domain', data['tenant'], data['vn_name']])
    try:
        current_rt_imp=vn.import_route_target_list.route_target
    except Exception as exp:
        current_rt_imp=""
    try:
        current_rt_exp=vn.export_route_target_list.route_target
    except Exception as exp:
        current_rt_exp=""

    if data['rt_import'] or data['rt_export']:
        if ( 
            not list(set(current_rt_imp).intersection(set(data['rt_import']))) == data['rt_import'] 
            and not list(set(current_rt_exp).intersection(set(data['rt_export']))) == data['rt_export']
        ):
            update_rt(vnc,data)
            string+=" Route Target "

    if data['network_policy']:
        if vn.get_network_policy_refs():
            if ( len(vn.network_policy_refs) == len(data['network_policy']) ):
                for x in range(len(vn.network_policy_refs)):
                    if not vn.network_policy_refs[x]['to'][2] in data['network_policy']:
                        update_netpol(vnc,data)
                        string+=" Network Policy "
                        break
            else:
                update_netpol(vnc,data)
                string+=" Network Policy"
        else:
            update_netpol(vnc,data)
            string+=" Network Policy"

    if data['subnet']:
            string+=update_subnet(vnc,data)
    
    if string is "":
        exit_module(False, False, "Nothing changed")
    else: 
        exit_module(False, True, string + " changed successfully")

def exit_module(error,has_changed,meta):
    if not error:
        print(json.dumps({ "changed" : has_changed, "meta" : meta }))
        #module.exit_json(changed=has_changed,msg=meta)
    else:
        print(json.dumps({ "failed" : "True", "msg" : meta }))
        #module.fail_json(changed=has_changed,msg=meta)

#create VN
def vn_ops_present(data):
    vnc = vnc_api.VncApi(
            username = data['user'],
            password = data['pass'],
            tenant_name = data['tenant'] ,
            api_server_host = data['contrail_api_url'],
            auth_host = data['keystone_public'],
            auth_port = data['keystone_port'])
    if not check_vn(vnc, data['tenant'], data['vn_name']):
        #create_vn(vnc, data)
        check(vnc,data,create_vn(vnc,data))
    else:
        check(vnc,data)

#delete VN
def vn_ops_absent(data):
    vnc = vnc_api.VncApi(
            username = data['user'],
            password = data['pass'],
            tenant_name = data['tenant'] ,
            api_server_host = data['contrail_api_url'],
            auth_host = data['keystone_public'],
            auth_port = data['keystone_port'])
    try:
        vnc.virtual_network_delete(fq_name = ['default-domain', data['tenant'], data['vn_name']])
    except Exception as exp:
        exit_module(False, False, "This error " + str(type(exp)) + " happened: " + exp())
    else:
        if not check_vn(vnc, data['tenant'], data['vn_name']):
            exit_module(True, True, "VN deleted successfully")
        else:
            exit_module(False, True, "VN should have been deleted but it is still present")

def main():
    fields = {
            "user": {"required": True, "type": "str"},
            "pass": {"required": True, "type": "str" },
            "contrail_api_url": {"required": True, "type": "str"},
            "keystone_public": {"required": True, "type": "str" },
            "keystone_port": {"required": True, "type": "str"},

            "tenant": {"default": "admin", "type": "str" },
            "vn_name": {"required": True, "type": "str" },
            "ipam": {"default": "default-network-ipam", "type": "str" },
            "rt_import": {"default": "None", "type": "list" },
            "rt_export": {"default": "None", "type": "list" },
            "network_policy": {"default": "None", "type": "list"},
            "subnet": {"default": "None", "type": "list"},
            "state": {
                    "default": "present",
                    "choices": ['present', 'absent'],
                    "type": 'str'
            }
    }
    choice_map = {
            "present": vn_ops_present,
            "absent": vn_ops_absent
    }
    # bad programming practice but needs rewriting the code logic or making an object to fix
    module = AnsibleModule(argument_spec=fields)
    choice_map.get(module.params['state'])(module.params)

if __name__ == '__main__':
    main()

